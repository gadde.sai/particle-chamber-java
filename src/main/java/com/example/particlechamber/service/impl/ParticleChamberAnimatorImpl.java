package com.example.particlechamber.service.impl;

import com.example.particlechamber.model.ParticleChamber;
import com.example.particlechamber.service.ParticleChamberAnimator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParticleChamberAnimatorImpl implements ParticleChamberAnimator {

    private List<String> animationSerquence;

    @Override
    public synchronized void animate(ParticleChamber particleChamber) {
        animationSerquence = new ArrayList<>();
        animationSerquence.add(particleChamber.serializeState());
        while (particleChamber.particleCount() > 0) {
            particleChamber.tick();
            animationSerquence.add(particleChamber.serializeState());
        }
    }

    @Override
    public synchronized List<String> getAnimationSequence() {
        return animationSerquence;
    }


}
