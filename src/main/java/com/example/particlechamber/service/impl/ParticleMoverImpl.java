package com.example.particlechamber.service.impl;

import com.example.particlechamber.model.Particle;
import com.example.particlechamber.model.Position;
import com.example.particlechamber.service.ParticleMover;
import org.springframework.stereotype.Component;

@Component
public class ParticleMoverImpl implements ParticleMover {

    @Override
    public Position move(Position position, Particle particle) {
        int currentPosition = position.getX();
        int newPosition;
        switch (particle.getDirection()) {
            case LEFT:
                newPosition = currentPosition - particle.getSpeed();
                break;
            case RIGHT:
                newPosition = currentPosition + particle.getSpeed();
                break;
            default:
                throw new IllegalStateException("Invalid direction");
        }
        return new Position(newPosition);
    }
}
