package com.example.particlechamber.service.impl;

import com.example.particlechamber.model.Particle;
import com.example.particlechamber.model.Position;
import com.example.particlechamber.service.ParticleStateSerializer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ParticleStateSerializerImpl implements ParticleStateSerializer {

    @Override
    public String serializeParticleState(Map<Position, List<Particle>> state) {
        StringBuilder stateBuilder = new StringBuilder();
        state.entrySet().stream()
                .forEach(entry -> {
                    if (entry.getValue().size() == 0) {
                        stateBuilder.append(".");
                    } else {
                        stateBuilder.append("X");
                    }
                });
        return stateBuilder.toString();
    }
}
