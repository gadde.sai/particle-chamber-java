package com.example.particlechamber.service;

import com.example.particlechamber.model.ParticleChamber;

import java.util.List;

public interface ParticleChamberAnimator {

    void animate(ParticleChamber particleChamber);

    List<String> getAnimationSequence();

}
