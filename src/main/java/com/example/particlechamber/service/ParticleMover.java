package com.example.particlechamber.service;

import com.example.particlechamber.model.Particle;
import com.example.particlechamber.model.Position;

public interface ParticleMover {
    Position move(Position position, Particle particle);
}
