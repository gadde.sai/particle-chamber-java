package com.example.particlechamber.service;

import com.example.particlechamber.model.Particle;
import com.example.particlechamber.model.Position;

import java.util.List;
import java.util.Map;

public interface ParticleStateSerializer {
    String serializeParticleState(Map<Position, List<Particle>> state);
}
