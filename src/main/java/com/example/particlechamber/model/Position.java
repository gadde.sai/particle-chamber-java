package com.example.particlechamber.model;

import java.util.Objects;

public class Position implements Comparable<Position> {
    private int x;

    public Position(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x);
    }

    @Override
    public int compareTo(Position position) {
        return Integer.compare(this.getX(), position.getX());
    }
}
