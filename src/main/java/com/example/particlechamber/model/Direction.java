package com.example.particlechamber.model;

public enum Direction {
    LEFT,
    RIGHT
}
