package com.example.particlechamber.model;

import com.example.particlechamber.service.ParticleMover;
import com.example.particlechamber.service.ParticleStateSerializer;

import java.util.*;

public class ParticleChamber {
    private Map<Position, List<Particle>> state;

    private ParticleMover particleMover;
    private ParticleStateSerializer serializer;

    public ParticleChamber(Map<Position, List<Particle>> state) {
        this.state = state;
    }

    public synchronized int particleCount() {
        return state.values().stream()
                .map(List::size)
                .mapToInt(i -> i)
                .sum();
    }

    public synchronized void tick() {
        Map<Position, List<Particle>> nextState = new TreeMap<>();
        state.entrySet().stream()
                .forEach(entry -> {
                    if (nextState.get(entry.getKey()) == null) {
                        nextState.put(entry.getKey(), new ArrayList<>());
                    }
                    entry.getValue()
                            .stream()
                            .forEach(particle -> {
                                Position newPosition = particleMover.move(entry.getKey(), particle);
                                if (newPosition.getX() >= 0 && newPosition.getX() < state.size()) {
                                    nextState.compute(newPosition, (pos, particles) -> {
                                        if (particles == null) {
                                            particles = new ArrayList<>();
                                        }
                                        particles.add(particle);
                                        return particles;
                                    });
                                }
                            });
                });

        state = nextState;
    }

    public String serializeState() {
        return serializer.serializeParticleState(state);
    }

    public void setSerializer(ParticleStateSerializer serializer) {
        this.serializer = serializer;
    }

    public void setParticleMover(ParticleMover particleMover) {
        this.particleMover = particleMover;
    }
}
