package com.example.particlechamber.model;

public class Particle {
    private Direction direction;
    private int speed;

    public Particle(Direction direction, int speed) {
        this.direction = direction;
        this.speed = speed;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getSpeed() {
        return speed;
    }
}
