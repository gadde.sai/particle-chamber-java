package com.example.particlechamber.http;

import com.example.particlechamber.http.dto.ParticleAnimationRequestDTO;
import com.example.particlechamber.http.factory.ConstantSpeedParticleChamberFactory;
import com.example.particlechamber.model.ParticleChamber;
import com.example.particlechamber.service.ParticleChamberAnimator;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class ParticleChamberAnimationController {

    private ConstantSpeedParticleChamberFactory particleChamberFactory;

    private ParticleChamberAnimator animator;

    public ParticleChamberAnimationController(ConstantSpeedParticleChamberFactory particleChamberFactory,
                                              ParticleChamberAnimator animator) {
        this.particleChamberFactory = particleChamberFactory;
        this.animator = animator;
    }

    @GetMapping(path = "animate", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> animateParticleChamber(@RequestBody ParticleAnimationRequestDTO particleAnimationRequest) {
        ParticleChamber particleChamber = particleChamberFactory
                .createParticleChamber(particleAnimationRequest.getInit(), particleAnimationRequest.getSpeed());

        animator.animate(particleChamber);

        return ResponseEntity.ok("{\"" + String.join("\",\n\"", animator.getAnimationSequence()) + "\"}");
    }
}
