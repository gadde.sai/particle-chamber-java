package com.example.particlechamber.http.factory;

import com.example.particlechamber.model.Direction;
import com.example.particlechamber.model.Particle;
import com.example.particlechamber.model.ParticleChamber;
import com.example.particlechamber.model.Position;
import com.example.particlechamber.service.ParticleMover;
import com.example.particlechamber.service.ParticleStateSerializer;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ConstantSpeedParticleChamberFactory {

    private ParticleMover particleMover;
    private ParticleStateSerializer serializer;

    public ConstantSpeedParticleChamberFactory(ParticleMover particleMover,
                                               ParticleStateSerializer serializer) {
        this.particleMover = particleMover;
        this.serializer = serializer;
    }

    public ParticleChamber createParticleChamber(String intialState, int speed) {
        Map<Position, List<Particle>> state = new TreeMap<>();
        for (int i = 0; i < intialState.length(); i++) {
            switch (intialState.charAt(i)) {
                case '.':
                    state.put(new Position(i), Collections.EMPTY_LIST);
                    break;
                case 'L':
                    state.put(new Position(i), Arrays.asList(new Particle(Direction.LEFT, speed)));
                    break;
                case 'R':
                    state.put(new Position(i), Arrays.asList(new Particle(Direction.RIGHT, speed)));
                    break;
                default:
                    throw new IllegalStateException("Invalid direction");
            }
        }

        ParticleChamber particleChamber = new ParticleChamber(state);
        particleChamber.setParticleMover(particleMover);
        particleChamber.setSerializer(serializer);
        return particleChamber;
    }
}
