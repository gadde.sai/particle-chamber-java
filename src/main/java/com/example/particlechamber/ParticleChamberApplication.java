package com.example.particlechamber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParticleChamberApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParticleChamberApplication.class, args);
	}

}
