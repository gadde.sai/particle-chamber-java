package com.example.particlechamber;

import com.example.particlechamber.http.dto.ParticleAnimationRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.RequestEntity;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ParticleChamberApplicationTests {
    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testCase1() {
        test("..R....", 2,
                "{\"..X....\",\n" +
                        "\"....X..\",\n" +
                        "\"......X\",\n" +
                        "\".......\"}");
    }

    @Test
    public void testCase2() {
        test("RR..LRL", 3,
                "{\"XX..XXX\",\n" +
                        "\".X.XX..\",\n" +
                        "\"X.....X\",\n" +
                        "\".......\"}");
    }

    @Test
    public void testCase3() {
        test("LRLR.LRLR", 2,
                "{\"XXXX.XXXX\",\n" +
                        "\"X..X.X..X\",\n" +
                        "\".X.X.X.X.\",\n" +
                        "\".X.....X.\",\n" +
                        "\".........\"}");
    }

    @Test
    public void testCase4() {
        test("RLRLRLRLRL", 10,
                "{\"XXXXXXXXXX\",\n" +
                        "\"..........\"}");
    }

    @Test
    public void testCase5() {
        test("...", 1,
                "{\"...\"}");
    }

    @Test
    public void testCase6() {
        test("LRRL.LR.LRR.R.LRRL.", 1,
                "{\"XXXX.XX.XXX.X.XXXX.\",\n" +
                        "\"..XXX..X..XX.X..XX.\",\n" +
                        "\".X.XX.X.X..XX.XX.XX\",\n" +
                        "\"X.X.XX...X.XXXXX..X\",\n" +
                        "\".X..XXX...X..XX.X..\",\n" +
                        "\"X..X..XX.X.XX.XX.X.\",\n" +
                        "\"..X....XX..XX..XX.X\",\n" +
                        "\".X.....XXXX..X..XX.\",\n" +
                        "\"X.....X..XX...X..XX\",\n" +
                        "\".....X..X.XX...X..X\",\n" +
                        "\"....X..X...XX...X..\",\n" +
                        "\"...X..X.....XX...X.\",\n" +
                        "\"..X..X.......XX...X\",\n" +
                        "\".X..X.........XX...\",\n" +
                        "\"X..X...........XX..\",\n" +
                        "\"..X.............XX.\",\n" +
                        "\".X...............XX\",\n" +
                        "\"X.................X\",\n" +
                        "\"...................\"}");
    }

    private void test(String init, int speed, String expected) {
        ParticleAnimationRequestDTO animationRequest = new ParticleAnimationRequestDTO();
        animationRequest.setInit(init);
        animationRequest.setSpeed(speed);
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(
                    HttpRequest.newBuilder()
                            .method("GET", HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(animationRequest)))
                            .header("Content-Type", "application/json")
                            .uri(URI.create("http://localhost:" + port + "/animate"))
                            .build(),
					HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(expected, response.body());
    }

}
